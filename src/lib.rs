
use fluent_builder::SharedFluentBuilder;
use elastic::{
	client::{
		requests::RequestInner,
		responses::NodesInfoResponse,
	},
	endpoints::Endpoint,
	http::{
		sender::{
			build_url,
			NodeAddress,
			NodeAddresses,
			NodeAddressesBuilder,
			PreRequestParams,
			RequestParams,
			SendableRequest,
			SendableRequestParams,
			Sender,
			sniffed_nodes::{
				SniffedNodesBuilder,
				NextOrRefresh,
			},
			TypedSender,
		},
		receiver::{
			IsOk,
			parse,
		},
	},
	Error,
};
use url::Url;
use js_promises::*;
use stdweb::{
	self,
	js,
	unstable::TryInto as _
};
use std::{
	error::Error as StdError,
	fmt,
};

/// ElasticSearch Client using JS `XMLHttpRequest`.
pub type StdwebClient = elastic::client::Client<StdwebSender>;

/// Builder for `StdwebClient`.
pub struct StdwebClientBuilder {
	nodes: NodeAddressesBuilder,
	params: SharedFluentBuilder<PreRequestParams>,
}
impl StdwebClientBuilder {
	pub fn new() -> Self {
		Self {
			nodes: NodeAddressesBuilder::default(),
			params: SharedFluentBuilder::new(),
		}
	}
	
	pub fn from_params(params: PreRequestParams) -> Self {
		Self {
			nodes: NodeAddressesBuilder::default(),
			params: SharedFluentBuilder::new().value(params),
		}
	}
	
	pub fn static_node(self, node: impl Into<NodeAddress>) -> Self {
		self.static_nodes(Some(node))
	}
	
	pub fn static_nodes<I, S>(mut self, nodes: I) -> Self
	where
		I: IntoIterator<Item = S>,
		S: Into<NodeAddress>,
	{
		let nodes = nodes.into_iter().map(|address| address.into()).collect();
		self.nodes = NodeAddressesBuilder::Static(nodes);
		self
	}
	
	pub fn sniff_nodes(mut self, builder: impl Into<SniffedNodesBuilder>) -> Self {
		self.nodes = self.nodes.sniff_nodes(builder.into());
		self
	}
	
	pub fn sniff_nodes_fluent(
		mut self,
		address: impl Into<NodeAddress>,
		builder: impl Fn(SniffedNodesBuilder) -> SniffedNodesBuilder + Send + 'static,
	) -> Self {
		self.nodes = self.nodes.sniff_nodes_fluent(address.into(), builder);
		self
	}
	
	pub fn params_fluent(
		mut self,
		builder: impl Fn(PreRequestParams) -> PreRequestParams + Send + 'static,
	) -> Self {
		self.params = self.params.fluent(builder).shared();
		
		self
	}
	
	pub fn build(self) -> Result<StdwebClient, Error> {
		let params = self.params.into_value(|| PreRequestParams::default());
		let sender = StdwebSender;
		
		let addresses = self.nodes.build(params);
		
		Ok(StdwebClient::from_sender(sender, addresses))
	}
}
impl Default for StdwebClientBuilder {
	fn default() -> Self {
		Self::new()
	}
}

/// ElasticSearch Sender using JS `XMLHttpRequest`.
#[derive(Clone,Debug)]
pub struct StdwebSender;

impl Sender for StdwebSender {
	type Body = StdwebBody;
	type Response = Promise<http::Response<Vec<u8>>, Error>;
	type Params = StdwebParams;
	
	fn send<TEndpoint, TParams, TBody>(
		&self,
		request: SendableRequest<TEndpoint, TParams, TBody>
	) -> Self::Response
	where
		TEndpoint: Into<Endpoint<'static, TBody>>,
		TBody: Into<Self::Body> + Send + 'static,
		TParams: Into<Self::Params> + Send + 'static,
	{
		let _correlation_id = request.correlation_id;
		let params = request.params.into();
		let Endpoint {
			url, method, body, ..
		} = request.inner.into();
		
		let params_promise: Promise<RequestParams, Error> = match params {
			SendableRequestParams::Value(params) => Promise::new_resolved(params),
			SendableRequestParams::Builder { params, builder } => {
				params.into().0
					.map(move |params| -> RequestParams { builder.into_value(move || params).into() })
			}
		};
		
		params_promise
			.and_then(move |params| {
				let url = Url::parse(&build_url(&url, &params))
					.map_err(elastic::error::request)?;
				let headers = params.get_headers();
				let body: Option<Vec<u8>> = body.map(|v| v.into().into());
				
				let req = js!{
					let req = new XMLHttpRequest();
					req.open(@{method.as_str()}, @{url.as_str()});
					req.responseType = "arraybuffer";
					return req;
				};
				
				for (k,v) in headers.iter() {
					js!{ @(no_return)
						@{&req}.setRequestHeader(@{k.as_str()}, @{v.to_str().expect("Non-string header value")});
					}
				}
				
				let send_promise = RawPromise::from_value(js!{
					return new Promise(function(resolve, reject) {
						@{&req}.addEventListener("load", resolve);
						@{&req}.addEventListener("abort", reject);
						@{&req}.addEventListener("error", reject);
					});
				}).then_to_typed(|res| {
					let event = res
						.map_err(|e| elastic::error::request(SendError{error_value: e}))?;
					
					let status: u16 = js!{ return @{&event}.target.status; }
						.try_into()
						.expect("load event.target.status was not an integer");
					let body: stdweb::web::TypedArray<u8> = js!{ return new Uint8Array(@{&event}.target.response); }
						.try_into()
						.expect("load event.target.response was not a typed array");
					let mut headers: String = js!{ return @{&event}.target.getAllResponseHeaders() }
						.try_into()
						.expect("load event.target.getAllResponseHeaders() was not a string");
					headers.push_str("\r\n\r\n"); // Add double-crlf so httparse knows it's done.
					
					let mut response_builder = http::Response::builder();
					response_builder.status(status);
					parse_headers(&headers, &mut response_builder);
					let response = response_builder.body(body.to_vec()).expect("Could not build response object");
					
					return Ok(PromiseOk::Immediate(response));
				});
				
				if let Some(body) = body {
					unsafe {
						let body_arr = stdweb::UnsafeTypedArray::<u8>::new(&body);
						js!{ @(no_return)
							@{&req}.send(@{body_arr});
						};
					}
				} else {
					js!{ @(no_return)
						@{&req}.send();
					};
				};
				
				return Ok(PromiseOk::Promise(send_promise));
			})
	}
	
	fn next_params(
		&self,
		addresses: &NodeAddresses,
	) -> Self::Params {
		match addresses {
			NodeAddresses::Static(addresses) => addresses.next().into(),
			NodeAddresses::Sniffed(sniffer) => {
				let refresher = match sniffer.next_or_start_refresh() {
					Ok(NextOrRefresh::Next(address)) => {
						return address.into();
					},
					Ok(NextOrRefresh::NeedsRefresh(r)) => r,
					Err(err) => {
						return Promise::new_rejected(err).into();
					},
				};
				
				let req = sniffer.sendable_request();
				let promise = self.send(req)
					.and_then(move |res| {
						let fresh_nodes = deserialize_response::<NodesInfoResponse>(res);
						refresher.update_nodes_and_next(fresh_nodes)
							.map(|v| PromiseOk::Immediate(v))
					});
				promise.into()
			}
		}
	}
}

impl<TReqInner> TypedSender<TReqInner> for StdwebSender
where
	TReqInner: RequestInner,
{
	type TypedResponse = Promise<TReqInner::Response, Error>;
	fn typed_send<TParams, TEndpoint, TBody>(
		&self,
		request: Result<SendableRequest<TEndpoint, TParams, TBody>, Error>,
	) -> Self::TypedResponse
	where
		TEndpoint: Into<Endpoint<'static, TBody>> + Send + 'static,
		TBody: Into<Self::Body> + Send + 'static,
		TParams: Into<Self::Params> + Send + 'static,
	{
		let sendable_req_promise = Promise::from_result(request);
		let this = self.clone();
		sendable_req_promise
			.and_then(move |req| Ok(PromiseOk::Promise(this.send(req))))
			.and_then(|response| {
				deserialize_response::<TReqInner::Response>(response)
					.map(|v| PromiseOk::Immediate(v))
			})
	}
}

/// Stdweb request parameters type, wrapper for a `Promise<RequestParams, Error>`
/// that has some `From` impls.
pub struct StdwebParams(pub Promise<RequestParams, Error>);
impl From<Promise<RequestParams, Error>> for StdwebParams {
	fn from(v: Promise<RequestParams, Error>) -> Self {
		Self(v)
	}
}
impl From<RequestParams> for StdwebParams {
	fn from(v: RequestParams) -> Self {
		Self(Promise::new_resolved(v))
	}
}
impl From<Result<RequestParams, Error>> for StdwebParams {
	fn from(v: Result<RequestParams, Error>) -> Self {
		Self(Promise::from_result(v))
	}
}
impl Into<Promise<RequestParams, Error>> for StdwebParams {
	fn into(self) -> Promise<RequestParams, Error> {
		self.0
	}
}

/// Stdweb request body type, wrapper for a `Vec<u8>` buffer
/// that has some `From` impls.
pub struct StdwebBody(pub Vec<u8>);
impl From<Vec<u8>> for StdwebBody {
	fn from(body: Vec<u8>) -> Self {
		Self(body)
	}
}
impl<'a> From<&'a [u8]> for StdwebBody {
	fn from(body: &'a [u8]) -> Self {
		let mut buf = vec![0; body.len()];
		buf.copy_from_slice(body);
		Self(buf)
	}
}
impl From<String> for StdwebBody {
	fn from(body: String) -> Self {
		Self(body.into_bytes())
	}
}
impl From<serde_json::Value> for StdwebBody {
	fn from(body: serde_json::Value) -> Self {
		Self(body.to_string().into())
	}
}
impl Into<Vec<u8>> for StdwebBody {
	fn into(self) -> Vec<u8> {
		self.0
	}
}

/// Helper function for parsing headers using httparse
fn parse_headers(header_str: &str, builder: &mut http::response::Builder) {
	let bytes = header_str.as_bytes();
	let mut headers_buf = vec![httparse::EMPTY_HEADER; 4];
	loop {
		match httparse::parse_headers(bytes, headers_buf.as_mut_slice()) {
			Ok(httparse::Status::Complete((_index, headers_slice))) => {
				for header in headers_slice {
					builder.header(header.name, header.value);
				}
				return;
			},
			Err(httparse::Error::TooManyHeaders) => {
				headers_buf.resize(headers_buf.len() * 2, httparse::EMPTY_HEADER);
				continue;
			},
			// The following should never happen, since the browser makes sure the headers
			// are properly formed.
			Ok(httparse::Status::Partial) => {
				panic!("httparse said this is a partial response but it's not");
			},
			Err(e) => {
				panic!("httparse could not parse headers response: {}", e);
			}
		}
	}
}

fn deserialize_response<T: IsOk + serde::de::DeserializeOwned + Send + 'static>(response: http::Response<Vec<u8>>) -> Result<T, Error> {
	parse()
		.from_slice(response.status(), response.body().as_slice())
		.map_err(|e| elastic::error::response(response.status(), e))
}

/// Error response from the `XMLHttpRequest` API.
#[derive(Debug,Clone)]
pub struct SendError {
	/// Object passed to the `error` or `abort` callback
	pub error_value: stdweb::Value
}
impl fmt::Display for SendError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.write_str("Could not complete request")
	}
}
impl StdError for SendError {}
