
use elastic_stdweb;
use js_promises::*;
use serde_json::{Value, json};
use stdweb::{js, console};


fn panic_hook(info: &std::panic::PanicInfo) {
	let mut msg = info.to_string();
	let stack = js!{
		let err = new Error();
		return err.stack;
	};
	msg.push_str("\n\nStack:\n\n");
	msg.push_str(stack.as_str().unwrap_or("<Error.stack was not a string>"));
	msg.push_str("\n\n");
	
	console!(error, msg);
}
fn set_panic_hook() {
	std::panic::set_hook(Box::new(panic_hook));
}

fn main() {
	if let Err(e) = run() {
		console!(error, e);
	}
}

fn run() -> Result<(), String> {
	set_panic_hook();
	let es = elastic_stdweb::StdwebClientBuilder::new()
		.static_node("http://localhost:9200")
		.build()
		.map_err(|e| format!("Could not create client: {}", e))?;
	
	let promise = es
		.search::<Value>()
		.index("_all")
		.body(json!({
			"query": {
				"query_string": {
					"query": "*"
				}
			}
		}))
		.send();
	
	promise.then::<(),(),_>(|result| {
		console!(log, "Got result");
		let res = match result {
			Ok(v) => v,
			Err(err) => {
				console!(error, format!("ES search failed: {}", err));
				return Ok(PromiseOk::Immediate(()));
			}
		};
		
		for hit in res.hits() {
			console!(log, format!("{:?}", hit));
		}
		
		return Ok(PromiseOk::Immediate(()));
	});
	
	Ok(())
}